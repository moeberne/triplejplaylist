//
//  TJPDownloader.m
//  TripleJPlaylist
//
//  Created by Moe Burney on 13/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

#import "TJPDownloader.h"
@interface TJPDownloader() <NSURLSessionDownloadDelegate>
@property (nonatomic, strong) NSURLSession* session;
@end

@implementation TJPDownloader

// Public method to initialize the downloader
- (id) initWithConfiguration: (NSURLSessionConfiguration*) config
{
    self = [super init];
    if (self)
    {
        NSURLSession* session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        self->_session = session;
    }
    return self;
}

// Public method to start a download
- (NSURLSessionTask*) download:(NSString*)s
             completionHandler:(void(^)(NSURL* url))ch
{
    NSURL* url = [NSURL URLWithString:s];
    NSMutableURLRequest* req = [NSMutableURLRequest requestWithURL:url];
    [NSURLProtocol setProperty:ch forKey:@"ch" inRequest:req];
    NSURLSessionDownloadTask* task = [self.session downloadTaskWithRequest:req];
    [task resume];
    return task;
}

// Delegate methods
-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
     didWriteData:(int64_t)bytesWritten
totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    ;;;
}

-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes
{
    ;;;
}

-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)location
{
    NSURLRequest* req = downloadTask.originalRequest;
    void(^ch)(NSURL* url) =
    [NSURLProtocol propertyForKey:@"ch" inRequest:req];
    NSHTTPURLResponse* response = (NSHTTPURLResponse*)downloadTask.response;
    NSInteger stat = response.statusCode;
    NSURL* url = nil;
    if (stat == 200)
        url = location;
    ch(url);
}

// Public method for client to call for memory management
- (void) cancelAllTasks
{
    [self.session invalidateAndCancel];
}

@end
