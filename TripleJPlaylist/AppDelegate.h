//
//  AppDelegate.h
//  TripleJPlaylist
//
//  Created by Moe Burney on 13/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

