//
//  main.m
//  TripleJPlaylist
//
//  Created by Moe Burney on 13/06/2015.
//  Copyright (c) 2015 Moe Burney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
